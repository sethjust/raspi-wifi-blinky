import sys
sys.path.append('/home/pi/raspi-wifi-blinky/BlinkyTape/')
from BlinkyTape import BlinkyTape

from time import sleep
import urllib2
import re
import datetime

MACS = re.compile(r"'xx:xx:xx:xx:(..):(..)'")

bt = BlinkyTape('/dev/ttyACM0')

url = "http://192.168.1.1/Info.live.htm"
request = urllib2.Request(url)
opener = urllib2.build_opener()

dim = 12

def now():
	return datetime.datetime.now().strftime("%Y-%m-%dT%H:%M:%S")

def clamp(val):
	return 255 if val > 255 else 0 if val < 0 else val

def toHex(c):
	return "#%02x%02x%02x" % c

def color(mac):
	return _color(mac, 0.45)

def _color(mac, yIdx):
	i = int(mac[:2], 16)
	j = int(mac[2:], 16)

	# xor to change up bit density
	i ^= 0b101010 # 0x2A
	j ^= 0b101010 # 0x2A	

	y = int(yIdx * 255)
	cb = i
	cr = j

	r = clamp(int(y + (1.402 * (cr - 128))))
	g = clamp(int(y - (0.344136 * (cb - 128)) - (0.714136 * (cr - 128))))
	b = clamp(int(y + (1.772 * (cb - 128))))

	r,g,b = (r/10, g/25, b/10) #TODO: dimming

	#print "%s (%x%x) gives #%02x%02x%02x (rgb %d, %d, %d)" % (mac, i, j, r, g, b, r, g, b)
	return (r, g, b);

def depart(mac):
	r, g, b = c = color(mac)
	print "Farewell %s (%s) at %s" % (mac, toHex(c), now())

	for i in range(bt.ledCount/2):
		for j in range(bt.ledCount/2):
			if j >= i:
				bt.sendPixel(r, g, b)
			else:
				bt.sendPixel(0,0,0)
		for j in range(bt.ledCount/2):
			if bt.ledCount/2 - j > i:
				bt.sendPixel(r, g, b)
			else:
				bt.sendPixel(0,0,0)
		bt.show()
		sleep(.1)

	off()
	sleep(.5)

def arrive(mac):
	r, g, b = c = color(mac)
	print "Welcome %s (%s) at %s" % (mac, toHex(c), now())

	for i in range(bt.ledCount/2):
		for j in range(bt.ledCount/2):
			if bt.ledCount/2 - j > i:
				bt.sendPixel(0,0,0)
			else:
				bt.sendPixel(r, g, b)
		for j in range(bt.ledCount/2):
			if j >= i:
				bt.sendPixel(0,0,0)
			else:
				bt.sendPixel(r, g, b)
		bt.show()
		sleep(.1)

	off()
	sleep(.5)

	for i in range(3):
		bt.displayColor(r, g, b)
		sleep(.5)
		off()
		sleep(.5)

def park(macs):
	arr = [(0,0,0) for i in range(bt.ledCount)]
	macs = sorted(macs)
	cs = map(color, macs)

	n = len(macs)
	for i in range(n):
		c = cs[i]
		j = (bt.ledCount/2) - n + (2*i)
		arr[j] = c
		arr[j + 1] = c

	bt.sendList(arr)
	
	sleep(5)

	arr[(bt.ledCount/2) - n - 1] = (dim, dim, 0)
	bt.sendList(arr)

def off():
	bt.displayColor(0,0,0)

try:
	bt.displayColor(0, dim, 0)
	sleep(0.5)

	present = None
	while True:
		try:
			raw = opener.open(request).read()

			line = filter(lambda ln: ln.startswith('{active_wireless::'), raw.split('\n'))
			if not len(line) or line is None:
				raise Exception("No data found")

			line = line[0]
			macs = map(lambda (a, b): a+b, MACS.findall(line))

			if present is None:
				print zip(macs, map(toHex, map(color, macs)))
				present = set()
				for mac in macs:
					present.add(mac)
			else:
				for mac in macs:
					if mac not in present:
						present.add(mac)
						arrive(mac)

				left = set()
				for mac in present:
					if mac not in macs:
						left.add(mac)
						depart(mac)
				for mac in left:
					present.remove(mac)

			park(macs)
		except Exception as e:
			print "ERROR:", e
			bt.displayColor(dim,0,0)
			sleep(5)
except KeyboardInterrupt:
	try:
		off()
	except:
		print "Clearing failed!"

	try:
		bt.close()
		print "Cleaned up!"
	except Exception as e:
		print "Cleanup failed!", e
