# raspi-wifi-blinky

Python script designed for use on a Raspeberry Pi, along with a BlinkyTape and a DD-WRT router (defaults to 192.168.1.1).
Polls the router for wireless client MAC addresses and indicates arrivals and departures.

## Setup

To set up, you need a Raspberry Pi runing Raspbian.
First install dependencies:

    sudo apt install python-serial git

Then clone the repo:

    cd /home/pi/
    git clone git@gitlab.com:sethjust/raspi-wifi-blinky.git
    git submodule init

If you want the script to run at boot, add the following line to /etc/rc.local:

    python /home/pi/raspi-wifi-blinky/blinky.py &

The trailing `&` is required to allow the script to run in the background while the computer keeps booting.
